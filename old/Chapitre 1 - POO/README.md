# js-ts

&nbsp;

## Programmation Orientée Objet en JavaScript

&nbsp;

La programmation orientée objet (POO ou OOP en anglais) est un paradigme de programmation qui consiste à modéliser des concepts du monde réel en utilisant des objets. Chaque objet possède des propriétés (ou attributs) et des comportements (appelés méthodes).

Voici comment la POO peut être mise en œuvre en JavaScript :

&nbsp;

### 1. Les classes :

&nbsp;

ES6 a introduit le concept de classes en JavaScript, qui permet de créer des objets de manière plus structurée et de définir des méthodes pour ces objets. Voici un exemple de classe en JavaScript :

&nbsp;

![Les classes en JS](../assets/class.png 'Les classes en JS')

&nbsp;

Le mot-clé _this_ est utilisé dans une classe en JavaScript pour faire référence à l'objet courant au sein de la classe. Il est souvent utilisé pour accéder aux propriétés et aux méthodes de l'objet courant, ou pour appeler des méthodes de l'objet courant à l'intérieur de la classe.

Dans l'exemple ci-dessus, la classe _Person_ définit une propriété _name_ et une méthode _sayHello()_. Lorsque la méthode _sayHello()_ est appelée sur l'objet _person_, le mot-clé _this_ est utilisé pour accéder à la propriété _name_ de l'objet courant et pour afficher son nom.

&nbsp;

### 2. Les héritages :

&nbsp;

JavaScript permet la création de classes dérivées (appelées sous-classes) qui héritent des propriétés et des méthodes de classes parentes (appelées aussi classes de base). Voici un exemple d'héritage en JavaScript :

&nbsp;

![L'héritage en JS](../assets/extends.png)

&nbsp;

### 3. Les interfaces :

&nbsp;

JavaScript n'a pas de concept d'interface native, mais il est possible de simuler des interfaces en utilisant des classes abstraites. Une classe abstraite est une classe qui ne peut pas être instanciée directement, mais qui peut être étendue par d'autres classes. Voici un exemple d'interface en JavaScript :

&nbsp;

![Interfaces en JS](../assets/interfaces.png 'Interfaces en JS')

&nbsp;

Voici les principaux points à retenir sur la Programmation Orientée Objet en JavaScript. Si vous souhaitez en connaître d'avantage, vous pouvez consulter la documentation de Mozilla à ce sujet [ici](https://developer.mozilla.org/fr/docs/Learn/JavaScript/Objects/Object-oriented_programming).

&nbsp;

[Chapitre suivant](../Chapitre%202%20-%20ES6/README.md)
