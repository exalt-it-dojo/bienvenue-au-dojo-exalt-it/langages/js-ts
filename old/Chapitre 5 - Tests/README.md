# js-ts

&nbsp;

## Comment tester son code en JS ?

&nbsp;

Le testing (ou test) est une étape cruciale dans le développement d'un logiciel. Il permet de vérifier que le code fonctionne correctement et qu'il respecte les spécifications et les exigences du projet. Il existe différents types de tests, mais dans cette formation, nous allons nous concentrer sur les tests unitaires en JavaScript.

&nbsp;

Un test unitaire est un type de test qui vérifie le bon fonctionnement d'une partie isolée du code, appelée unité. Une unité peut être une fonction, une méthode, une classe, ou même une simple expression. Le but est de vérifier que cette unité se comporte comme prévu dans toutes les situations possibles.
Les tests unitaires sont généralement écrits par les développeurs eux-mêmes, et sont exécutés automatiquement à chaque modification du code, afin de s'assurer que les modifications n'ont pas cassé quelque chose qui fonctionnait déjà.
Il existe de nombreux outils de testing en JavaScript, mais nous allons nous concentrer sur les deux plus connus : Jest et Mocha.

&nbsp;

### Jest

&nbsp;

Jest est un framework de testing développé par Facebook. Il est facile à prendre en main et fournit de nombreux outils pour écrire des tests unitaires en JavaScript.

&nbsp;

Voici un exemple de test unitaire avec Jest :

&nbsp;

![Testing Jest toBe](../assets/testing-jest-be.png)

&nbsp;

Dans cet exemple, nous avons défini une fonction sum qui calcule la somme de deux nombres, et un test qui vérifie que sum(1, 2) renvoie bien 3.

La fonction test permet de définir, via une chaîne de charactère, ce que l'on souhaite tester ici.

&nbsp;

Jest possède aussi d'autres vérifications possibles :

&nbsp;

- vérifier qu'un tableau contient bien une valeur :

&nbsp;

![Testing Jest toContain](../assets/testing-jest-contain.png)
&nbsp;

- vérifier la taille d'un tableau :

  &nbsp;

![Testing Jest toHaveLength](../assets/testing-jest-length.png)

&nbsp;

- vérifier qu'un objet possède bien une propriété :

&nbsp;

![Testing Jest toHaveProperty](../assets/testing-jest-property.png)
&nbsp;

- vérifier qu'une erreur soit levée:

&nbsp;

![Testing Jest toThrow](../assets/testing-jest-error.png)
&nbsp;

Ces vérifications peuvent être précédées de .not pour tester le contraire.

&nbsp;

### Mocha

&nbsp;

Mocha est un framework de testing plus flexible que Jest. Il ne fournit pas autant de fonctionnalités que Jest, mais il est plus facile à intégrer avec d'autres outils de développement.

Voici le même exemple de test de la fonction sum avec Mocha :

&nbsp;

![Testing Mocha equal](../assets/testing-mocha-equal.png)

&nbsp;

Dans cet exemple, nous avons utilisé la fonction describe pour définir un groupe de tests, et la fonction it pour définir un test individuel. Nous avons également utilisé la fonction assert.equal pour vérifier que le résultat de l'appel à sum(1, 2) est égal à 3.

&nbsp;

Voici quelques exemples d'autres méthodes d'assertion que vous pouvez utiliser dans vos tests Mocha:

&nbsp;

- vérifier qu'une variable possède une valeur :

&nbsp;

![Testing Mocha ok](../assets/testing-mocha-ok.png)

&nbsp;

- vérifier le type d'une variable :

&nbsp;

![Testing Mocha strict](../assets/testing-mocha-strict.png)

&nbsp;

- vérifier que deux objets soient identiques :

&nbsp;

![Testing Mocha deep](../assets/testing-mocha-deep.png)

&nbsp;

- vérifier que l'on reçoit une erreur :

&nbsp;

![Testing Mocha deep](../assets/testing-mocha-error.png)

&nbsp;

Pour aller plus loin avec le testing, n'hésitez pas à lire la doc officielle de [Jest](https://jestjs.io/fr/), et de [Mocha](https://mochajs.org/)

&nbsp;

[Chapitre suivant](../Chapitre%206%20-%20CI%20CD/README.md)
