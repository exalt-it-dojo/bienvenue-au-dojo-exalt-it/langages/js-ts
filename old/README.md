# js-ts

&nbsp;

Cette formation a pour but de vous donner les bases en JavaScript nécessaires afin de pouvoir par la suite exercer une mission dans ce langage.

&nbsp;

## Sujets qui seront évoqués dans la formation

&nbsp;

- [Programmation Orientée Objet en JavaScript](/Chapitre%201%20-%20POO/README.md)
- [Fondamentaux d'ES6](/Chapitre%202%20-%20ES6/README.md)
- [Différence entre JS et TS](/Chapitre%203%20-%20TS/README.md)
- [Connaissance des différents frameworks JS](/Chapitre%204%20-%20Frameworks/README.md)
- [Comment tester son projet JS ?](/Chapitre%205%20-%20Tests/README.md)
- [Implémenter sa CI et sa CD en JS](/Chapitre%206%20-%20CI%20CD/README.md)

&nbsp;

## À votre tour !

&nbsp;

Maintenant que vous avez suivi cette formation, il est temps de pratiquer ce que vous avez appris via les katas front et back en JS :

- [Kata Front - Rendering Event](https://gitlab.com/exalt-it-dojo/katas/frontjs-rendering-events)
- [Kata Back - REST API](https://gitlab.com/exalt-it-dojo/katas/nodejs-rest-api)

&nbsp;

N'oubliez pas que vous serez attendus non seulement sur la fonctionnalité de votre projet, mais aussi sur sa lisibilité, son architecture, et les tests implémentés. La CI/CD et les bonnes pratiques Git ne sont pas des critères que nous prenons en compte dans la relecture.
