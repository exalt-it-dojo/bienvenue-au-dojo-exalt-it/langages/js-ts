# js-ts

&nbsp;

## Les différences majeures entre JS et TS

&nbsp;

TypeScript est un langage de programmation open source développé et maintenu par Microsoft. Il s'agit d'un sur-ensemble de JavaScript qui ajoute des fonctionnalités de typage statique au langage. Cela signifie que, contrairement à JavaScript, où les variables peuvent être de n'importe quel type, TypeScript impose un type aux variables, ce qui permet de mieux vérifier la validité du code et de détecter les erreurs de programmation avant de compiler le code.

Voici la liste des principaux changements entre JavaScript et TypeScript :

&nbsp;

### 1. La déclaration de constantes et de variables :

&nbsp;

Voici un exemple de déclaration de variables en TypeScript :


&nbsp;

![Déclaration de variables en TS](../assets/ts-variables.png)

&nbsp;

Dans cet exemple, nous déclarons trois variables : _message_, _count_ et _isActive_. Nous spécifions le type de chaque variable en utilisant les types de base de TypeScript (_string_, _number_ et _boolean_). Si nous tentons de assigner une valeur de type incompatible à l'une de ces variables, TypeScript nous signale une erreur de compilation.

TypeScript supporte également la déclaration de types de données personnalisés, tels que des objets et des tableaux. Voici un exemple de déclaration d'un type d'objet avec TypeScript :

&nbsp;

Voici les types principaux rencontrés en TS :

    number: représente les nombres, qu'ils soient entiers ou à virgule flottante.

    string: représente les chaînes de caractères, comme du texte.

    boolean: représente les valeurs booléennes, soit true ou false.

    null et undefined: représentent les valeurs nulles ou non définies.

    object: représente tout objet, qu'il soit primitif ou complexe.

    Array: représente un tableau de valeurs, où toutes les valeurs sont du même type.

    Tuple: représente un tableau de valeurs, où les types de chaque élément peuvent être différents.

    enum: représente une énumération de valeurs.

    any: représente n'importe quel type possible, ce qui peut être pratique pour des situations où le type de variable n'est pas connu à l'avance.

Il existe également des types plus avancés comme interface, class, void, never, unknown, etc. qui permettent d'exprimer des concepts plus complexes en Typescript.

&nbsp;

![Déclaration d'objets en TS](../assets/type.png)

&nbsp;

Dans cet exemple, nous définissons un type _User_ qui décrit un objet ayant deux propriétés : _name_ (de type _string_) et _age_ (de type _number_). Nous pouvons ensuite utiliser ce type pour déclarer une variable _user_ de type _User_.


La manière la plus simple de renvoyer plusieurs valeurs en JavaScript est de les regrouper dans un objet ou un tableau et de les décomposer en variables distinctes à l'aide de la destructuration. Par exemple, si vous souhaitez renvoyer deux valeurs numériques, vous pouvez les regrouper dans un tableau et les décomposer en deux variables distinctes comme suit :

> const [valeur1, valeur2] = [1, 2];
console.log(valeur1); // affiche 1
console.log(valeur2); // affiche 2

&nbsp;

### 2. La déclaration de fonctions :

&nbsp;

En TypeScript, vous pouvez typer les fonctions en spécifiant le type des arguments et le type de retour de la fonction.

Voici un exemple simple de fonction typée en TypeScript :

&nbsp;

![Déclaration de fonctions en TS](../assets/ts-functions.png)

&nbsp;

Dans cet exemple, nous déclarons une fonction _add_ qui prend deux arguments de type _number_ et renvoie un résultat du même type. Lorsque nous appelons la fonction, nous passons deux nombres en argument et stockons le résultat dans une variable _result_. TypeScript vérifie que les arguments sont bien des nombres et que le résultat est également un nombre, ce qui garantit que le code est exécuté de manière correcte.

Voici un autre exemple qui utilise des types de données personnalisés avec une fonction :

&nbsp;

![Déclaration de fonctions en TS](../assets/ts-functions2.png)

&nbsp;

Dans cet exemple, nous définissons un type _User_ qui décrit un objet ayant deux propriétés : _name_ (de type _string_) et _age_ (de type _number_). Nous déclarons ensuite une fonction _createUser_ qui prend deux arguments de type _string_ et _number_, respectivement, et qui renvoie un objet de type _User_. Lorsque nous appelons la fonction _createUser_, nous passons deux chaînes de caractères en argument et stockons le résultat dans une variable _user_.

TypeScript vérifie que les arguments sont bien des chaînes de caractères et que le résultat est un objet de type _User_, ce qui garantit, encore une fois, que le code est exécuté de manière correcte.

&nbsp;

### 3. Les interfaces :

&nbsp;

Les interfaces en TypeScript permettent de définir un contrat pour les objets de votre code. Elles spécifient les propriétés et les méthodes que doit implémenter un objet pour être considéré comme valide.

Voici un exemple simple d'interface en TypeScript :

&nbsp;

![Interfaces en TS](../assets/ts-interfaces.png)

&nbsp;

Dans cet exemple, nous déclarons une interface _Shape_ qui définit une propriété sides et une méthode _area()_. Nous déclarons ensuite une classe _Triangle_ qui implémente cette interface. Cela signifie que la classe _Triangle_ doit définir une propriété sides et une méthode _area()_, et que ces propriétés et méthodes doivent respecter le contrat défini par l'interface.

&nbsp;

### 4. Les génériques :

&nbsp;

Les génériques en TypeScript permettent de définir des fonctions, des classes et d'autres structures de données qui peuvent être utilisées avec différents types de données. Ils permettent de créer des structures de données flexibles et réutilisables sans avoir à écrire de nouvelles versions de ces structures pour chaque type de donnée spécifique.

Voici un exemple simple de générique en TypeScript :

&nbsp;

![Déclaration de génériques en TS](../assets/ts-generic.png)

&nbsp;

Dans cet exemple, nous déclarons une fonction _identity_ qui prend un argument de type T et renvoie un résultat de type T. Le type T est défini comme un générique, ce qui signifie qu'il peut être remplacé par n'importe quel type de donnée lorsque la fonction est appelée. Dans l'exemple ci-dessus, nous appelons la fonction _identity_ en spécifiant le type _string_, ce qui signifie que l'argument et le résultat seront des chaînes de caractères.

Voici un autre exemple qui utilise les génériques avec une classe :

&nbsp;

![Déclaration de génériques en TS](../assets/ts-generic2.png)

&nbsp;

Dans cet exemple, nous déclarons une classe _DataStorage_ qui peut être utilisée avec n'importe quel type de donnée. La classe définit une propriété data qui est un tableau de données de type T, ainsi que deux méthodes : _addItem_ et _getItem_. Lorsque nous créons une instance de la classe _DataStorage_, nous spécifions le type de donnée que nous souhaitons utiliser (ici, _string_). Cela signifie que toutes les données stockées dans l'instance de la classe seront des chaînes de caractères, et que tous les arguments et résultats des méthodes de la classe seront également des chaînes de caractères.

&nbsp;

Pour rentrer plus en détail sur les ajouts de TypeScript, vous pouvez consulter la documentation officielle de TypeScript, notamment avec ses cheat sheets [ici](https://www.typescriptlang.org/cheatsheets).

&nbsp;

[Chapitre suivant](../Chapitre%204%20-%20Frameworks/README.md)
