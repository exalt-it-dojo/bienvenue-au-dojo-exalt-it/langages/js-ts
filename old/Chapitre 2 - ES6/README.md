# js-ts

&nbsp;

## <ins>Les fondamentaux d'ES6</ins>

&nbsp;

ES6 (officiellement appelé ECMAScript 2015) est une version importante de JavaScript qui a été publiée en 2015. Elle apporte de nombreuses nouvelles fonctionnalités et améliorations au langage de programmation JavaScript. Voici les changements les plus importants apportés par ES6 :

&nbsp;

### 1. La déclaration de variables :

&nbsp;

ES6 ajoute les mots-clés _let_ et _const_ pour déclarer des variables. _let_ permet de déclarer une variable de portée limitée (comme _var_ en ES5), tandis que _const_ permet de déclarer une constante (comme décrit ci-dessus). Voici un exemple de déclaration de variables en ES6 :

&nbsp;

![Déclarer ses variables en ES6](../assets/variables.png 'Déclarer ses variables en ES6')

&nbsp;

En revanche, le fait de déclarer une variable en tant que constante ne la rend pas immuable. Par exemple, pour le cas d'un objet :

    const obj = {} obj.property = true 
    // On ne peut pas réassigner obj mais on peut modifier 
    // des propriétés

&nbsp;

### 2. Les Classes :

&nbsp;

ES6 introduit le concept de classes en JavaScript, qui permet de créer des objets de manière plus structurée et de définir des méthodes pour ces objets. En ES5, il était possible de simuler des classes en utilisant des fonctions constructeurs et le prototype, mais cette approche était moins intuitive. Voici un exemple de classe définie en ES6 :

&nbsp;

![Créer une classe en ES6](../assets/class.png 'Créer une classe en ES6')

&nbsp;

### 3. Les fonctions fléchées :

&nbsp;

ES6 introduit les fonctions fléchées, qui sont une syntaxe concise pour définir des fonctions. Elles ont un comportement similaire aux fonctions traditionnelles, mais elles n'ont pas leur propre contexte de "this", ce qui peut être utile dans certains cas. Voici un exemple de fonction fléchée en ES6 :

&nbsp;

![Créer des fonctions en ES6](../assets/arrow-function.png 'Créer des fonctions en ES6')

&nbsp;

### 3. Les paramètres par défaut :

&nbsp;

ES6 permet de définir des valeurs par défaut pour les arguments d'une fonction. Si aucune valeur n'est fournie pour un argument, la valeur par défaut sera utilisée à la place. Voici un exemple de fonction avec des paramètres par défaut en ES6 :

&nbsp;

![Paramètre par défaut](../assets/default-parameter.png 'Paramètre par défaut')

&nbsp;

### 4. Le _destructuring_ :

&nbsp;

ES6 introduit des opérateurs de décomposition qui permettent de décomposer (ou _unpack_ en anglais) des tableaux et des objets en variables séparées. Voici un exemple de _destructuring_ en ES6 avec un tableau, et avec un objet :

&nbsp;

La manière la plus simple de renvoyer plusieurs valeurs en JavaScript est de les regrouper dans un objet ou un tableau et de les décomposer en variables distinctes à l'aide de la destructuration. Par exemple, si vous souhaitez renvoyer deux valeurs numériques, vous pouvez les regrouper dans un tableau et les décomposer en deux variables distinctes comme suit :

    const [valeur1, valeur2] = [1, 2];

    console.log(valeur1); // affiche 1
    console.log(valeur2); // affiche 2

![Destructuring tableau](../assets/array-destructuring.png 'Destructuring tableau')

&nbsp;

![Destructuring objet](../assets/object-destructuring.png 'Destructuring objet')

&nbsp;

### 5. Les promesses :

&nbsp;

ES6 introduit les promesses, qui permettent de gérer de manière plus efficace les opérations asynchrones (c'est-à-dire qui ne se déroulent pas de manière séquentielle). Les promesses représentent une valeur qui peut être disponible maintenant, dans le futur, ou jamais. Voici un exemple de promesse en ES6 :

Les différents états d'une promesse JavaScript sont "pending", "fulfilled" et "rejected". La promesse est dans un état "en attente" lorsqu'elle est créée et que son résultat n'est pas encore connu. Elle passe à l'état "résolue" lorsqu'une opération asynchrone réussie et que le résultat est connu. Elle passe à l'état "rejetée" si une opération asynchrone échoue et que le résultat est connu.

&nbsp;

![Les promesses en ES6](../assets/promises.png 'Les promesses en ES6')

&nbsp;

### 6. Les générateurs :

&nbsp;

ES6 ajoute les générateurs, qui sont des fonctions spéciales qui peuvent être "suspendues" et "reprises" à différents moments de leur exécution. Les générateurs permettent de créer des itérateurs personnalisés et de simplifier la gestion de l'état dans les algorithmes complexes. Voici un exemple de générateur en ES6 :

&nbsp;

![Les générateurs en ES6](../assets/generator.png 'Les générateurs en ES6')

&nbsp;

Ce ne sont là que les fonctionnalités majeures introduites par ES6. Si vous souhaitez en savoir plus sur les changements apportés par ES6, je vous recommande de consulter la documentation officielle disponible [ici](https://262.ecma-international.org/6.0/).

&nbsp;

[Chapitre suivant](../Chapitre%203%20-%20TS/README.md)
