# js-ts

&nbsp;

## Les principaux frameworks en JS

&nbsp;

Dans le domaine du développement informatique, un framework (ou cadre de développement en français) est un ensemble de bibliothèques, de modules et d'outils qui fournissent une structure de base pour le développement de logiciels. Il s'agit d'un ensemble d'abstractions qui permettent de simplifier le développement en fournissant des fonctionnalités courantes et des structures de données préconçues.

Le but d'un framework est d'accélérer le processus de développement en fournissant une base solide sur laquelle les développeurs peuvent construire leur application. En utilisant un framework, les développeurs peuvent éviter de réinventer la roue en développant des fonctionnalités de base à partir de zéro. Le framework fournit également une structure cohérente pour l'ensemble de l'application, ce qui facilite la maintenance et la mise à jour à long terme.

&nbsp;

Voici un aperçu de certains des frameworks JavaScript les plus utilisés et de ce qu'ils peuvent faire :

&nbsp;

## Frameworks front-end

&nbsp;

### ReactJS

&nbsp;

React.js est l'un des frameworks JavaScript les plus populaires. Il est utilisé pour construire des interfaces utilisateur pour les applications web. Il est créé et maintenu par Facebook.
Le développement en React consiste à créer des composants réutilisables. Pour créer un composant, vous devez créer une classe ou une fonction qui retourne du JSX (une syntaxe similaire à HTML qui permet de créer des éléments d'interface utilisateur).

Voici un exemple de composant React simple:

&nbsp;

![Déclaration d'un composant React](../assets/frameworks-react-component.png)

&nbsp;

Une autre fonctionnalité utile de React est la gestion des états dans les composants, grâce aux hooks.

Les hooks sont des fonctions qui permettent d'ajouter des fonctionnalités à un composant fonctionnel sans avoir besoin de le transformer en une classe. Il existe plusieurs hooks prédéfinis dans React, mais les deux plus couramment utilisés sont useState et useEffect.

&nbsp;

### VueJS

&nbsp;

VueJS est un framework JavaScript open-source utilisé pour construire des interfaces utilisateur réactives et dynamiques, et pour la création d'applications web à grande échelle.

Souvent comparé à React, ils possèdent tous les deux des similitudes : composants, gestion du cycle de vie via hooks, gestion du code HTML.

Cependant, comparé à React, les composants se font via des instances Vues. Ces instances prenent en entrée un objet de configuration qui décrit l'état initial de l'application, ainsi que des fonctions et des options pour personnaliser son comportement.

Voici un exemple de déclaration d'une instance Vue :

&nbsp;

![Déclaration d'une instance Vue](../assets/frameworks-vue-instance.png)

&nbsp;

Un autre point intéressant concernant VueJS sont les directives spécifiques à ce langage. Les directives sont des attributs spéciaux que l'on peut ajouter à des éléments HTML pour leur donner des fonctionnalités supplémentaires. Les directives les plus couramment utilisées dans VueJS sont v-bind (pour lier des propriétés), v-if (pour afficher ou masquer des éléments en fonction d'une condition), et v-for (pour itérer sur une liste d'éléments).

&nbsp;

### AngularJS

&nbsp;

AngularJS est un framework JavaScript open source qui est utilisé pour la création d'applications web dynamiques et pour la manipulation de l'interface utilisateur. Il est créé et maintenu par Google.

Les concepts clés revenant le plus en Angular sont les contrôleurs et les services. Les contrôleurs sont des fonctions JavaScript qui gèrent l'état de votre application, tandis que les services sont utilisés pour effectuer des tâches récurrentes ou partager des données entre les différentes parties de votre application.

Voici comment déclarer un service en Angular :

&nbsp;

![Déclaration d'un service Angular](../assets/frameworks-angular-service.png)

&nbsp;

Le contrôleur lui, va appeler le service associé, comme ceci :

&nbsp;

![Déclaration d'un controller Angular](../assets/frameworks-angular-controller.png)

&nbsp;

## Frameworks back-end

&nbsp;

### Express.JS

&nbsp;

Express.js est un framework JavaScript minimaliste et flexible pour Node.js qui permet de créer des applications web rapides et robustes.
La création d'une API est simple et rapide avec ce framework, grâce, entre autres, à une multitude de méthodes utilitaires HTTP.

Voici une application express basique :

&nbsp;

![Déclaration d'une application Express](../assets/frameworks-express.png)

&nbsp;

Dans cet exemple, l'application Express.js écoute sur le port 3000. Lorsqu'un utilisateur accède à la racine de l'application, l'application renvoie "Hello World!".

&nbsp;

### NestJS

&nbsp;

Nest.js est un framework Node.js basé sur TypeScript, qui permet de développer des applications backend évolutives, modulaires et performantes. Il utilise des concepts provenant de différents frameworks comme Angular, avec les modules, contrôleurs, et services.

Exemple de contrôleur en Nest :

&nbsp;

![Déclaration d'un contrôleur Nest](../assets/frameworks-nest-controller.png)

&nbsp;

Exemple de service en Nest :

&nbsp;

![Déclaration d'un service Nest](../assets/frameworks-nest-service.png)

&nbsp;

Si vous souhaitez vous renseigner un peu plus sur un de ces framworks, voici leurs documentations officielles :

- [ReactJS](https://fr.reactjs.org/)
- [VueJS](https://vuejs.org/)
- [AngularJS](https://angularjs.org/)
- [Express.JS](https://expressjs.com/)
- [NestJS](https://nestjs.com/)

&nbsp;

[Chapitre suivant](../Chapitre%205%20-%20Tests/README.md)
