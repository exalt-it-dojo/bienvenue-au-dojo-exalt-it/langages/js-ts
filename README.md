# 🖥 Credo JS/TS - Entretiens

Vous trouverez ici les questions d'entretiens à poser pour évaluer le niveau technique d'un candidat en Javascript / Typescript

Les questions sont déclinées en trois niveau et par appétence : 
- Junior (sortie d'école à 2 ans d'XP)
- Confirmé (3 ans à 6 ans d'XP)
- Senior (6 ans d'XP et plus)

&nbsp;

# 🖼️ Frontend

## 🎓 [Junior](./interview/frontend/JUNIOR.md)

Les notions à contrôler sont les suivantes : 

- Base de git
- Concepts généraux de JavaScript/TypeScript
- Bases de Programmation Orienté Objet
- Bases d'Architecture Logicielle 
- Bases de l'HTML/CSS
- Bases du Clean Code
- Bases de design d'API REST
- Connaissance des différents cas d'asynchronisme
- Connaissances des différents frameworks front-end
- Compréhension des Design Pattern
- Bases de tests

## 🧪 [Confirmé](./interview/frontend/INTERMEDIATE.md)

Les notions à contrôler sont les suivantes : 

- Principes SOLID
- Connaissance des différents protocoles de communication
- Component Testing, E2E Testing
- Connaissances de state management
- Gestion des performances de rendering
- Bases web vitals
- Bases analytics
- Programmation fonctionnelle
- Island architecture/micro front-end
- Connaissances avancées d'un framework ou lib front-end
- Bases AAA
- Bases de conteneurs
- Connaissance de Design System

## 🧙‍♂️ [Senior/Archi](./interview/frontend/SENIOR.md)

Les notions à contrôler sont les suivantes : 

- AB Test
- Maitrise de features flag
- Maitrise de Design System

&nbsp;

# ⚒️ Backend

## 🎓 [Junior](./interview/backend/JUNIOR.md)

Les notions à contrôler sont les suivantes : 

- Base de git
- Concepts généraux de JavaScript/TypeScript
- Bases de Programmation Orienté Objet
- Bases d'Architecture Logicielle 
- Bases du Clean Code
- Bases de design d'API REST/SOAP
- Connaissance des différents cas d'asynchronisme
- Connaissances des différents frameworks back-end
- Compréhension des Design Pattern
- Bases de design des bases de données (SQL, NoSQL)
- Bases de tests
- Bases de CI/CD

## 🧪 [Confirmé](./interview/backend/INTERMEDIATE.md)

Les notions à contrôler sont les suivantes : 

- Principes SOLID
- Connaissance des différents protocoles de communication
- Connaissances avancées des différents types d'API
- Programmation fonctionnelle
- Concepts généraux de Node
- Concepts généraux de conteneurs
- Programmation distribuée
- Programmation concurentielle
- Connaissances avancées d'un framework ou lib back-end
- Bases AAA
- Requêtes BDD avancées (SQL et NoSQL)
- Connaissances avancées en CI/CD
- Connaissances avancées des différentes architectures

## 🧙‍♂️ [Senior/Archi](./interview/backend/SENIOR.md)

Les notions à contrôler sont les suivantes : 

- Profiling
- Connaissances avancées AAA
- Maitrise de l'environnemment Node
- Maitrise de versionning d'API
- Maitrise de features flag